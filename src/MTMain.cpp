// MTMain.cpp
//
// Copyright (c) 2008-2013 Tristan Matthews <le.businessman@gmail.com>
//               2010 Alexandre Quessy <alexandre@quessy.net>
//
// based on:
// MPI_main.cpp
// by Mark Zadel
//
// This file is part of Polygnome.
//
// Polygnome is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Polygnome is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Polygnome.  If not, see <http://www.gnu.org/licenses/>.

#include "MTApplication.h"
#include "sigint.h"

int main(int argc, char **argv)
{
    sigint::registerHandlers();

    // user just wanted help or version info
    if (MTApplication::parseOptions(argc, argv))
        return 0;

    // instantiate the application and start it up
    MTApplication app(&argc, &argv);
    return 0;
}

