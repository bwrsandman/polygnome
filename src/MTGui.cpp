// MTGui.cpp
//
// Copyright (c) 2008-2013 Tristan Matthews <le.businessman@gmail.com>
//               2010 Alexandre Quessy <alexandre@quessy.net>
//               2007 Tim Mayberry (wrote user_config_directory() in Ardour3)
//               2002 gtkmm development team
//
// parts derived from:
// MPI_GuiRootWindow.cpp by Mark Zadel
//
// This file is part of Polygnome.
//
// Polygnome is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Polygnome is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Polygnome.  If not, see <http://www.gnu.org/licenses/>.

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif
#include "MTGui.h"
#include "MTAudio.h"
#include <gdk/gdkkeysyms.h>
#include <iostream>
#include <giomm.h>
#include <gtkmm/main.h>
#include <gtkmm/table.h>
#include <gtkmm/aboutdialog.h>
#include <gtkmm/actiongroup.h>
#include <gtkmm/filefilter.h>
#include <gtkmm/filechooser.h>
#include <gtkmm/filechooserdialog.h>
#include <gtkmm/uimanager.h>
#include <gtkmm/frame.h>
#include <gtkmm/stock.h>
#include "MTEvent.h"
#include "sigint.h"
#include "MTBeatWidget.h"
#include "MTMeterMapping.h"

namespace {
    const char * const SESSION_FILE_EXT = ".txt";
    const char * const MASTER_KEY = "is_master";
    const char * const GAIN_KEY = "gain";
    const char * const MUTE_KEY = "mute";
    const char * const TICKER_KEY = "ticker_active";
    const char * const FREQ_KEY = "freq";
    const char * const METER_KEY = "meter";

    std::string userConfigDir()
    {
        // adopt freedesktop standards, and put files in ~/.config/polygnome
        return Glib::build_filename(Glib::get_user_config_dir(), PACKAGE);
    }

    void createConfigDir()
    {
        try {
            Glib::RefPtr<Gio::File> dir(Gio::File::create_for_path(userConfigDir()));
            if (not dir->query_exists())
                dir->make_directory_with_parents();
        } catch (const Glib::Exception &ex) {
            std::cerr << "Exception caught: " << ex.what() << std::endl;
        }
    }

    std::string userConfigFilename()
    {
        const std::string package(PACKAGE);
        return Glib::build_filename(userConfigDir(), package + SESSION_FILE_EXT);
    }

    void saveConfigFile(const std::string &path, Glib::KeyFile &keyFile)
    {
        // File writing code derived from
        // gtkmm-documentation/trunk/examples/book/giomm/write_file/main.cc
        try {
            Glib::RefPtr<Gio::File> file(Gio::File::create_for_path(path));

            if (!file) {
                std::cerr << "Gio::File::create_for_path() returned an " <<
                             "empty RefPtr." << std::endl;
                return;
            }

            Glib::RefPtr<Gio::FileOutputStream> stream;

            // If the file exists already then replace it.
            // Otherwise, create it:
            if (file->query_exists())
                stream = file->replace();
            else
                stream = file->create_file();

            if (!stream) {
                std::cerr << "Gio::File::create_file() returned an empty "
                          << "RefPtr." << std::endl;
                return;
            }

            if (!stream->write(keyFile.to_data()))
                std::cerr << "Gio::InputStream::write() wrote 0 bytes." <<
                             std::endl;

            // Close the stream to make sure that changes are written now,
            // instead of just when the stream goes out of scope.
            // For instance, when using Gio::File::replace(), the file is only
            // actually replaced during close() or when the stream is destroyed.
            stream->close();
            stream.reset(); // Stream can't be used after we have closed it.
        } catch (const Glib::Exception& ex) {
            std::cerr << "Exception caught: " << ex.what() << std::endl;
        }
    }
    using std::tr1::ref;
    using std::tr1::bind;
} // end anonymous namespace

void MTGui::sendEvent(const MTEvent::Event &ev)
{
    const int MAX_TRIES = 100;
    int tries = 0;
    for (; !audio_.incomingEvents().can_write() and tries < MAX_TRIES; ++tries)
        usleep(10000);
    if (tries == MAX_TRIES)
        std::cerr << "Could not write to ring buffer!" << std::endl;
    else
        audio_.incomingEvents().write(ev);
}

void MTGui::onBpmChanged()
{
    int value = static_cast<int>(bpmButton_.get_value());
    sendEvent(bind(&MTEvent::bpm, ref(audio_), value / masterMeter_));
}

void MTGui::onFreqChanged(int index)
{
    int value = static_cast<int>(freqButtons_[index].get_value());
    sendEvent(bind(&MTEvent::freq, ref(audio_), index, value));
}

void MTGui::onMeterChanged(int index)
{
    int value = static_cast<int>(meterButtons_[index].get_value());
    beatWidgets_[index].setMeter(value);
    onMasterClicked(index); // in case this is the master
    sendEvent(bind(&MTEvent::meter, ref(audio_), index, value));
}

void MTGui::onGainChanged(double val, int index)
{
    sendEvent(bind(&MTEvent::gain, ref(audio_), index, val));
}

void MTGui::onMuteChanged(int index)
{
    gdouble gain = 0.0;
    if (not muteButtons_[index].get_active()) {
        gainButtons_[index].set_sensitive(true);
        beatWidgets_[index].setActive(true);
        gain = gainButtons_[index].get_value(); // get actual gain
    } else {
        gainButtons_[index].set_sensitive(false);
        beatWidgets_[index].setActive(false);
    }
    sendEvent(bind(&MTEvent::gain, ref(audio_), index, gain));
}

void MTGui::onMasterClicked(int index)
{
    // The row selected as "Master" will determine the meter to which the BPM
    // corresponds.
    if (masterButtons_[index].get_active()) {
        masterMeter_ = static_cast<int>(meterButtons_[index].get_value());
        onBpmChanged();
    }
}

void MTGui::onStartChanged()
{
    if (startButton_.get_active()) {
        startButton_.set_label("Stop");
        sendEvent(bind(&MTEvent::start, ref(audio_)));
    } else {
        startButton_.set_label("Start");
        sendEvent(bind(&MTEvent::stop, ref(audio_)));
    }
}

void MTGui::onMenuOpen()
{
    Gtk::FileChooserDialog dialog("Please choose a file",
                                  Gtk::FILE_CHOOSER_ACTION_OPEN);
    dialog.set_transient_for(*this);

    // Add response buttons the the dialog:
    dialog.add_button(Gtk::Stock::CANCEL, Gtk::RESPONSE_CANCEL);
    dialog.add_button(Gtk::Stock::OPEN, Gtk::RESPONSE_OK);

    // Add filters, so that only certain file types can be selected:

    Glib::RefPtr<Gtk::FileFilter> filterSessions(Gtk::FileFilter::create());
    filterSessions->set_name("Session files");
    std::string pattern("*");
    pattern += SESSION_FILE_EXT;
    filterSessions->add_pattern(pattern);
    dialog.add_filter(filterSessions);

    Glib::RefPtr<Gtk::FileFilter> filterAny(Gtk::FileFilter::create());
    filterAny->set_name("All");
    pattern = "*";
    filterAny->add_pattern(pattern);
    dialog.add_filter(filterAny);

    // Show the dialog and get a user response:
    if (dialog.run() == Gtk::RESPONSE_OK) {
        std::string filename(dialog.get_filename());
        loadSettings(filename);
    }
}

void MTGui::onMenuSave()
{
    Gtk::FileChooserDialog dialog("Save", Gtk::FILE_CHOOSER_ACTION_SAVE);
    dialog.set_transient_for(*this);

    // Add response buttons the the dialog:
    dialog.add_button(Gtk::Stock::CANCEL, Gtk::RESPONSE_CANCEL);
    dialog.add_button(Gtk::Stock::SAVE, Gtk::RESPONSE_OK);

    // Add filters, so that only certain file types can be selected:
    Glib::RefPtr<Gtk::FileFilter> filter_sessions(Gtk::FileFilter::create());
    filter_sessions->set_name("Session files");
    filter_sessions->add_pattern("*.txt");
    dialog.add_filter(filter_sessions);

    // Show the dialog and get a user response:
    if (dialog.run() == Gtk::RESPONSE_OK) {
        std::string filename(dialog.get_filename());
        if (filename.find(SESSION_FILE_EXT) == std::string::npos)
            filename += SESSION_FILE_EXT;
        saveSettings(filename);
    }
}
void MTGui::onMenuQuit()
{
    sendEvent(bind(&MTEvent::quit, ref(audio_))); 
    get_application()->remove_window(*this);
}

// Reference:
// http://mail.gnome.org/archives/gtkmm-list/2007-January/msg00308.html
void MTGui::onMenuAbout()
{
    if (about_.get() == 0) {
        about_.reset(new Gtk::AboutDialog);
        std::vector<Glib::ustring> authors;
        authors.push_back("Tristan Matthews <le.businessman@gmail.com>");
        authors.push_back("Alexandre Quessy <alexandre@quessy.net>");
        authors.push_back("Charles Li <chuck@mixed-metaphors.com>");
        about_->set_program_name(PACKAGE_NAME);
        about_->set_authors(authors);
        about_->set_comments("A polyrhythmic metronome");
        about_->set_copyright("Copyright (c) 2008-2014 Tristan Matthews");
        about_->set_license(PACKAGE_NAME " is licensed under GNU General "
                           "Public License (GPL) version 3 or later.");
        about_->set_version(PACKAGE_VERSION);
    }
    about_->run();
    about_->hide(); // this makes the dialog disappear when close is pressed
}

void MTGui::createFileMenu()
{
    Glib::RefPtr<Gtk::ActionGroup> actionGroup(Gtk::ActionGroup::create());
    Glib::RefPtr<Gtk::UIManager> UIManager(Gtk::UIManager::create());

    //File menu:
    actionGroup->add(Gtk::Action::create("FileMenu", "File"));
    actionGroup->add(Gtk::Action::create("FileOpen", "Open Session"),
                     sigc::mem_fun(*this, &MTGui::onMenuOpen));
    actionGroup->add(Gtk::Action::create("FileSave", "Save Session"),
                     sigc::mem_fun(*this, &MTGui::onMenuSave));
    actionGroup->add(Gtk::Action::create("LoadDefaults",
                     Gtk::Stock::REVERT_TO_SAVED, "Load Defaults"),
                     sigc::mem_fun(*this, &MTGui::loadDefaults));
    actionGroup->add(Gtk::Action::create("Quit", Gtk::Stock::QUIT),
                     sigc::mem_fun(*this, &MTGui::onMenuQuit));
    actionGroup->add(Gtk::Action::create("HelpMenu", "Help"));
    actionGroup->add(Gtk::Action::create("About", Gtk::Stock::ABOUT),
                     sigc::mem_fun(*this, &MTGui::onMenuAbout));

    UIManager->insert_action_group(actionGroup);

    // Layout the actions in a menubar and toolbar:
    const Glib::ustring ui_info =
        "<ui>"
        "  <menubar name='MenuBar'>"
        "    <menu action='FileMenu'>"
        "      <menuitem action='FileOpen'/>"
        "      <menuitem action='FileSave'/>"
        "      <menuitem action='LoadDefaults'/>"
        "      <menuitem action='Quit'/>"
        "    </menu>"
        "    <menu action='HelpMenu'>"
        "      <menuitem action='About'/>"
        "    </menu>"
        "  </menubar>"
        "</ui>";

    try {
        UIManager->add_ui_from_string(ui_info);
    } catch (const Glib::Error& ex) {
        std::cerr << "building menus failed: " <<  ex.what();
    }

    //Get the menubar and toolbar widgets, and add them to a container widget:
    Gtk::Widget* menubar = UIManager->get_widget("/MenuBar");
    if (menubar)
        vbox_.pack_start(*menubar, Gtk::PACK_SHRINK);

    add_accel_group(UIManager->get_accel_group());
}

void MTGui::loadDefaults()
{
    // make 0th ticker the master
    masterButtons_[0].set_active();

    const double DEFAULT_BPM = 60.0;
    bpmButton_.set_value(DEFAULT_BPM);

    // start with first three tickers only
    for (size_t i = 0; i != MTMeterMapping::NUM_TICKERS; ++i) {
        gainButtons_[i].set_value(1.0);
        if (i >= 3)
            muteButtons_[i].set_active(true);
        else {
            muteButtons_[i].set_active(false);
            beatWidgets_[i].setActive(true);
        }
        freqButtons_[i].set_value(100 * (1 + i));
        meterButtons_[i].set_value(MTMeterMapping::DEFAULT_MAPPING[i]);
    }
}

bool MTGui::loadSettings(const std::string &settingsPath)
{
    Glib::KeyFile keyFile;
    try {
        keyFile.load_from_file(settingsPath);
        // global settings
        bpmButton_.set_value(keyFile.get_integer("global", "bpm"));

        // ticker-specific settings
        for (size_t i = 0; i != MTMeterMapping::NUM_TICKERS; ++i) {
            std::string tickerGroup("Ticker ");
            tickerGroup += '0' + i;
            masterButtons_[i].set_active(keyFile.get_boolean(tickerGroup, MASTER_KEY));
            gainButtons_[i].set_value(keyFile.get_double(tickerGroup, GAIN_KEY));
            bool muted = keyFile.get_boolean(tickerGroup, MUTE_KEY);
            muteButtons_[i].set_active(muted);
            beatWidgets_[i].setActive(not muted);
            freqButtons_[i].set_value(keyFile.get_integer(tickerGroup, FREQ_KEY));
            int meter = keyFile.get_integer(tickerGroup, METER_KEY);
            meterButtons_[i].set_value(meter);
        }
    } catch (const Glib::KeyFileError &e) {
        std::cerr << e.what() << std::endl;
        return false;
    } catch (const Glib::FileError &e) {
        // We don't care if it's the defaults file, maybe we never created it
        if (settingsPath != userConfigFilename())
            std::cerr << e.what() << std::endl;
        return false;
    }
    return true;
}

void MTGui::saveSettings(const std::string &settingsPath) const
{
    Glib::KeyFile keyFile;

    // global settings
    keyFile.set_integer("global", "bpm", bpmButton_.get_value());

    // ticker-specific settings
    for (size_t i = 0; i != MTMeterMapping::NUM_TICKERS; ++i) {
        std::string tickerGroup("Ticker ");
        tickerGroup += '0' + i;
        keyFile.set_boolean(tickerGroup, MASTER_KEY, masterButtons_[i].get_active());
        keyFile.set_double(tickerGroup, GAIN_KEY, gainButtons_[i].get_value());
        keyFile.set_boolean(tickerGroup, MUTE_KEY, muteButtons_[i].get_active());
        keyFile.set_integer(tickerGroup, FREQ_KEY, freqButtons_[i].get_value());
        keyFile.set_integer(tickerGroup, METER_KEY, meterButtons_[i].get_value());
    }
    saveConfigFile(settingsPath, keyFile);
}

void MTGui::createWidgets()
{
    /* VBox:
     *     Table:
     *     freq | gain | mute | meter | master | beatWidget
     *     freq | gain | mute | meter | master | beatWidget
     *     freq | gain | mute | meter | master | beatWidget
     *     freq | gain | mute | meter | master | beatWidget
     *     freq | gain | mute | meter | master | beatWidget
     *
     * BPM
     * Start/Stop
     */

    createFileMenu();
    bpmButton_.set_range(1.0, 1024.0);
    bpmButton_.set_increments(1.0, 1.0);
    add(vbox_);
    const int ROWS = MTMeterMapping::NUM_TICKERS + 1;
    const int COLS = 5;
    Gtk::Table *table = Gtk::manage(new Gtk::Table(ROWS, COLS));

    Gtk::Label *freq_label = Gtk::manage(new Gtk::Label("Freq."));
    Gtk::Label *meter_label = Gtk::manage(new Gtk::Label("Meter"));
    Gtk::Label *master_label = Gtk::manage(new Gtk::Label("Master"));
    Gtk::Label *ticker_label = Gtk::manage(new Gtk::Label("Beats"));

    const int X_PAD = 10;
    table->attach(*freq_label, 0, 1, 0, 1, Gtk::SHRINK, Gtk::SHRINK, X_PAD);
    table->attach(*meter_label, 3, 4, 0, 1, Gtk::SHRINK, Gtk::SHRINK, X_PAD);
    table->attach(*master_label, 4, 5, 0, 1, Gtk::SHRINK, Gtk::SHRINK, X_PAD);
    table->attach(*ticker_label, 5, 6, 0, 1, Gtk::SHRINK, Gtk::SHRINK, X_PAD);

    // make sure radio buttons are all in the same group
    Gtk::RadioButtonGroup group = masterButtons_[0].get_group();
    for (size_t i = 1; i != MTMeterMapping::NUM_TICKERS; ++i)
        masterButtons_[i].set_group(group);

    for (size_t i = 0; i != MTMeterMapping::NUM_TICKERS; ++i) {
        int leftAttach = 0;
        int rightAttach = 1;
        // freq buttons
        freqButtons_[i].set_range(1, 17000);
        freqButtons_[i].set_increments(1.0, 1.0);
        freqButtons_[i].signal_value_changed().connect(
                sigc::bind<int>(sigc::mem_fun(*this,
                        &MTGui::onFreqChanged), i));
        table->attach(freqButtons_[i], leftAttach++, rightAttach++, i + 1,
                      i + 2, Gtk::SHRINK, Gtk::SHRINK);

        // gain buttons
        gainButtons_[i].signal_value_changed().connect(sigc::bind<int>(sigc::mem_fun(*this,
                        &MTGui::onGainChanged), i));
        table->attach(gainButtons_[i], leftAttach++, rightAttach++, i + 1,
                      i + 2, Gtk::SHRINK, Gtk::SHRINK);

        // mute buttons
        muteButtons_[i].signal_toggled().connect(sigc::bind<int>(sigc::mem_fun(*this,
                        &MTGui::onMuteChanged), i));
        table->attach(muteButtons_[i], leftAttach++, rightAttach++, i + 1,
                      i + 2, Gtk::SHRINK, Gtk::SHRINK);

        // meter widgets
        meterButtons_[i].set_range(1, 256);
        meterButtons_[i].set_increments(1.0, 1.0);
        meterButtons_[i].signal_value_changed().connect(
                sigc::bind<int>(sigc::mem_fun(*this,
                        &MTGui::onMeterChanged), i));
        table->attach(meterButtons_[i], leftAttach++, rightAttach++, i + 1,
                      i + 2, Gtk::SHRINK);

        // master radio buttons
        table->attach(masterButtons_[i], leftAttach++, rightAttach++, i + 1,
                      i + 2, Gtk::SHRINK, Gtk::SHRINK);
        masterButtons_[i].signal_toggled().connect(
                sigc::bind<int>(sigc::mem_fun(*this,
                        &MTGui::onMasterClicked), i));

        // visual tickers
        table->attach(beatWidgets_[i], leftAttach, rightAttach, i + 1, i + 2,
                      Gtk::EXPAND | Gtk::FILL, Gtk::EXPAND | Gtk::FILL);
        beatWidgets_[i].show();
    }
    Gtk::Frame *bpmFrame = Gtk::manage(new Gtk::Frame("BPM"));
    bpmFrame->add(bpmButton_);

    vbox_.pack_start(*table);
    vbox_.pack_start(*bpmFrame, Gtk::PACK_SHRINK);
    vbox_.pack_start(startButton_, Gtk::PACK_SHRINK);
}

MTGui::MTGui(stk::MTAudio &audio) :
    audio_(audio),
    startButton_("Start"),
    bpmButton_(),
    about_(),
    masterMeter_(1.0)
{
    using sigc::slot;
    using sigc::connection;
    set_title(PACKAGE_NAME);
#define ICON_PATH "pixmaps/polygnome.svg"
    const char *paths [] = {ICON_PATH, "/usr/local/share/" ICON_PATH,
                            "/usr/share/" ICON_PATH};
#undef ICON_PATH
    const size_t PATHS_LEN = sizeof(paths) / sizeof(paths[0]);
    for (size_t i = 0, done = false; !done and i < PATHS_LEN; ++i) {
        try {
            done = set_default_icon_from_file(paths[i]);
        } catch (const Glib::Error &e) {}
    }

    const int DEFAULT_WIDTH = 480;
    const int DEFAULT_HEIGHT = 290;
    set_size_request(DEFAULT_WIDTH, DEFAULT_HEIGHT);

    createWidgets();

    slot<bool> pollSlot(sigc::mem_fun(*this, &MTGui::pollMessages));

    const int TIMEOUT_VALUE = 50; // ms
    // This is where we connect the slot to the Glib::signal_timeout()
    connection pollConnection = Glib::signal_timeout().
        connect(pollSlot, TIMEOUT_VALUE);

    // When the button receives the "clicked" signal, it will call the
    // on_button_clicked() method defined below.
    startButton_.signal_clicked().
        connect(sigc::mem_fun(*this, &MTGui::onStartChanged));

    bpmButton_.signal_value_changed().
        connect(sigc::mem_fun(*this, &MTGui::onBpmChanged));

    if (not loadSettings(userConfigFilename()))
        loadDefaults();

    // intercept key press events, filter out the space bar
    Gtk::Main::signal_key_snooper().connect(sigc::mem_fun(*this, &MTGui::onKeyPressEvent));
    show_all_children();
}

bool MTGui::onKeyPressEvent(Gtk::Widget * /*widget*/, GdkEventKey *eventKey)
{
    if (eventKey->keyval == GDK_KEY_space and eventKey->type == GDK_KEY_PRESS) {
        startButton_.set_active(!startButton_.get_active());
        return true;
    } else {
        return false;
    }
}

MTGui::~MTGui()
{
    createConfigDir();
    saveSettings(userConfigFilename());
    sendEvent(bind(&MTEvent::quit, ref(audio_)));
}

bool MTGui::pollMessages()
{
    if (sigint::interrupted()) {
        std::cerr << "Interrupted, quitting..." << std::endl;
        onMenuQuit();
        return false;
    }

    // grabs up to MAX_EVENTS messages
    for (size_t i = 0; audio_.outgoingEvents().can_read(); ++i) {
        MTBeatEvent e(audio_.outgoingEvents().read());
        e.update(beatWidgets_);
    }

    return true;
}

