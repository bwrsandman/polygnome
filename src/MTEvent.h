// MTEvent.h
//
// Copyright (c) 2008-2013 Tristan Matthews <le.businessman@gmail.com>
//
// This file is part of Polygnome.
//
// Polygnome is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Polygnome is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License

#ifndef MTEVENT_H_
#define MTEVENT_H_

#include "MTEventFwd.h"
#include <cstddef>

namespace stk {
    class MTAudio;
}

class MTBeatWidget;

namespace MTEvent {
    void start(stk::MTAudio &audio);
    void stop(stk::MTAudio &audio);
    void quit(stk::MTAudio &audio);
    void bpm(stk::MTAudio &audio, int bpm);
    void freq(stk::MTAudio &audio, size_t id, int freq);
    void gain(stk::MTAudio &audio, size_t id, double gain);
    void mute(stk::MTAudio &audio, size_t id, bool mute);
    void meter(stk::MTAudio &audio, size_t id, int meter);
}

class MTBeatEvent {
    public:
        MTBeatEvent(size_t id = 0, size_t beat = 0) : id_(id), beat_(beat)
    {}
        void update(MTBeatWidget widgets[]);
    private:
        int id_;
        int beat_;
};

#endif // MTEVENT_H_
