// MTApplication.cpp
//
// Copyright (c) 2008-2013 Tristan Matthews <le.businessman@gmail.com>
//               2010 Alexandre Quessy <alexandre@quessy.net>
//
// This file is part of Polygnome.
//
// Polygnome is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Polygnome is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Polygnome.  If not, see <http://www.gnu.org/licenses/>.

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif
#include "MTApplication.h"
#include "MTAudio.h"
#include "MTGui.h"
#include <tr1/memory>
#include <gtkmm/main.h>
 
// The MTAudio object owns the message queues, must be created
// before and destroyed after the MTGUI object
MTApplication::MTApplication(int *argc, char ***argv)
#if GTK_CHECK_VERSION(3, 4, 0)
: Gtk::Application(*argc, *argv, "org." PACKAGE)
#endif
{
    using std::tr1::shared_ptr;
    // Initialise GTK
    Gtk::Main kit(argc, argv);
    shared_ptr<stk::MTAudio> audio(stk::MTAudio::create());
    MTGui gui(*audio);

    audio->start();
    // this hangs here -- calls gtkMainLoop(), which doesn't return.
    run(gui, *argc, *argv);
}

/// returns true if program should exit, false otherwise
bool MTApplication::parseOptions(int argc, char **argv)
{
    Glib::init();
    if (!Glib::thread_supported())
        Glib::thread_init();

    Glib::OptionContext context;

    Glib::OptionGroup group("group", "description", "help");
    Glib::OptionEntry entry;
    entry.set_long_name("version");
    entry.set_short_name('v');
    entry.set_description("Print version information");
    bool version = false;
    group.add_entry(entry, version);
    context.set_main_group(group);

    try {
        context.parse(argc, argv);
    } catch(const Glib::Error& ex) {
        std::cout << ex.what() << std::endl;
    }

    if (version) {
        std::cout << PACKAGE_STRING << std::endl;
        return true;
    }
    return false;
}

