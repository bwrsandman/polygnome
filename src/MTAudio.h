// MTAudio.h
//
// Copyright (c) 2008-2013 Tristan Matthews <le.businessman@gmail.com>
//               2010 Alexandre Quessy <alexandre@quessy.net>
//
// based on:
// MPI_Audio.h
// by Mark Zadel
//
// This file is part of Polygnome.
//
// Polygnome is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Polygnome is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Polygnome.  If not, see <http://www.gnu.org/licenses/>.
//
//  Description:
// 	Handles audio functionality.

#ifndef MTAUDIO_H_
#define MTAUDIO_H_

#undef VERSION
#include <stk/RtAudio.h>

#include <vector>
#include <tr1/memory>
#include "noncopyable.h"

#include "ringbuffer.h"

#include "MTEventFwd.h"
class MTBeatEvent;

namespace stk {
class MTTicker;

class MTAudio {
    public:
        /// This method exists because we need to do some configuration before creating Stk objects.
        static MTAudio *create();
        ~MTAudio();
        void quit() { quitFlag_ = true; }

        void start();
        /// should ONLY be called from the audio thread
        std::vector<std::tr1::shared_ptr<stk::MTTicker> >& getTickers();

        // shared between main thread and audio thread
        RingBuffer<MTBeatEvent> &outgoingEvents() { return outgoingEvents_; }
        RingBuffer<MTEvent::Event> &incomingEvents() { return incomingEvents_; }

        MTAudio();
        int tickBuffer(void *out, unsigned int bufferSz);

    private:
        RingBuffer<MTBeatEvent> outgoingEvents_;
        RingBuffer<MTEvent::Event> incomingEvents_;
        bool quitFlag_;
        static int tickBufferCb(void *out, void * /*in*/, unsigned int bufferSz,
                double /*streamtime*/, RtAudioStreamStatus /*status*/,
                void *data);

        RtAudio dac_;
        // only used and modified in the audio thread.
        // only allocated/initialized/deallocated in main thread to avoid
        // unbounded calls in the audio callback
        std::vector<std::tr1::shared_ptr<MTTicker> > tickers_;
        NON_COPYABLE(MTAudio);
};
}    // namespace stk

#endif // MTAUDIO_H_

