// MTEvent.cpp
//
// Copyright (c) 2008-2013 Tristan Matthews <le.businessman@gmail.com>
//               2010 Alexandre Quessy <alexandre@quessy.net>
//
// This file is part of Polygnome.
//
// Polygnome is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Polygnome is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License

#include <vector>

#include "MTEvent.h"
#include "MTAudio.h"
#include "MTTicker.h"
#include "MTBeatWidget.h"
#include "MTMeterMapping.h"

typedef std::vector<std::tr1::shared_ptr<stk::MTTicker> > Tickers;
typedef Tickers::iterator TickerIter;

void MTEvent::start(stk::MTAudio &audio)
{
    Tickers &tickers = audio.getTickers();
    for (TickerIter t = tickers.begin(); t != tickers.end(); ++t)
        (*t)->setActiveFlag(true);
}

void MTEvent::stop(stk::MTAudio &audio)
{
    Tickers &tickers = audio.getTickers();
    for (TickerIter t = tickers.begin(); t != tickers.end(); ++t)
        (*t)->setActiveFlag(false);
}

void MTEvent::bpm(stk::MTAudio &audio, int bpm)
{
    Tickers &tickers = audio.getTickers();
    for (TickerIter t = tickers.begin(); t != tickers.end(); ++t)
        (*t)->bpm(bpm);
}

void MTEvent::freq(stk::MTAudio &audio, size_t id, int freq)
{
    audio.getTickers().at(id)->freq(freq);
}

void MTEvent::meter(stk::MTAudio &audio, size_t id, int meter)
{
    audio.getTickers().at(id)->setRatio(meter);
}

void MTEvent::gain(stk::MTAudio &audio, size_t id, double gain)
{
    audio.getTickers().at(id)->gain(gain);
}

void MTEvent::mute(stk::MTAudio &audio, size_t id, bool mute)
{
    audio.getTickers().at(id)->setMute(mute);
}

void MTEvent::quit(stk::MTAudio &audio)
{
    audio.quit();
}

void MTBeatEvent::update(MTBeatWidget widgets[])
{
    widgets[id_].setBeat(beat_);
}
