// MTTicker.h
//
// Copyright (c) 2008-2013 Tristan Matthews <le.businessman@gmail.com>
//               2010 Alexandre Quessy <alexandre@quessy.net>
//
// based on:
// MPI_AudioSynth.h
// by Mark Zadel
//
// This file is part of Polygnome.
//
// Polygnome is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Polygnome is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Polygnome.  If not, see <http://www.gnu.org/licenses/>.

#ifndef MTTICKER_H_
#define MTTICKER_H_

#include <stk/Generator.h>
#include <stk/BiQuad.h>

#include "MTImpulse.h"
#include "ringbuffer.h"
#include "MTEvent.h"
#include "noncopyable.h"

namespace stk {

class MTTicker : public Generator {
    public:
        //! Class constructor
        MTTicker(RingBuffer<MTBeatEvent> &messageQueue, int id);

        //! Class destructor.
        virtual ~MTTicker();

        void setActiveFlag(bool value);
        bool isActive() const;
        void reset();
        void sampleRateChanged(StkFloat /*newRate*/, StkFloat /*oldRate*/);
        void bpm(int newBpm);
        void freq(int newFreq);
        void gain(double newGain);
        void setMute(bool mute);
        void setRatio(double newRatio);

        StkFloat tick();
        StkFrames& tick(StkFrames &frames, unsigned int /*channel*/);

    private:
        int counter_;
        int period_;            // beatlength in samples
        double measureLength_;  // measureLength in samples, double to
                                // avoid loss of precision
        MTImpulse impulse_;
        BiQuad filter_;
        int bpm_;               // beats per minute
        double ratio_;          // ratio of bpm to the master bpm
        int id_;                // ratio of bpm to the master bpm
        StkFloat lastOutput_;
        bool active_;
        int beat_;
        int cachedFrequency_;
        RingBuffer<MTBeatEvent> &messageQueue_;
        NON_COPYABLE(MTTicker);
};
        

inline StkFloat MTTicker::tick()
{
    if (active_) {
        while (counter_ >= measureLength_) {
            counter_ -= measureLength_;
            beat_ = 0;
        }
        // it's a beat
        if (counter_ % period_ == 0) {
            impulse_.excite();

            if (not impulse_.isMuted() and messageQueue_.can_write())
                messageQueue_.write(MTBeatEvent(id_, beat_));

            ++beat_;
        }

        if (impulse_.isSilent() or impulse_.isMuted()) {
            lastOutput_ = 0.0;
        } else {
            static const double ANTI_DENORMAL = 1e-18;
            lastOutput_ = ANTI_DENORMAL + filter_.tick(impulse_.tick());
        }

        ++counter_;
    } else {
        lastOutput_ = 0.0;
    }

    return lastOutput_;
}

// TODO: implement this
inline StkFrames& MTTicker::tick(StkFrames& frames, unsigned int /*channel*/)
{
    throw("unimplemented");
    return frames;
}

} // namespace stk

#endif // MTTICKER_H_
