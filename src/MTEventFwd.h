// MTEventFwd.h
//
// Copyright (c) 2008-2013 Tristan Matthews <le.businessman@gmail.com>
//
// This file is part of Polygnome.
//
// Polygnome is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Polygnome is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Polygnome.  If not, see <http://www.gnu.org/licenses/>.

#ifndef MTEVENT_FWD_H_
#define MTEVENT_FWD_H_

#include <tr1/functional>

namespace MTEvent {
    typedef std::tr1::function<void()> Event;
}

#endif // MTEVENT_FWD_H_
