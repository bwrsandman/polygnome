// MTMeterMapping.h
//
// Copyright (c) 2008-2013 Tristan Matthews <le.businessman@gmail.com>
//               2010 Alexandre Quessy <alexandre@quessy.net>
//
// This file is part of Polygnome.
//
// Polygnome is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Polygnome is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License

#ifndef MTMETER_MAPPING_
#define MTMETER_MAPPING_

#include <cstddef>

namespace MTMeterMapping {
    const size_t NUM_TICKERS = 5;
    const double DEFAULT_MAPPING[] = {1.0, 2.0, 3.0, 5.0, 7.0};
}

#endif // MTMETER_MAPPING_
