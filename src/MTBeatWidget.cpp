// MTBeatWidget.cpp
//
// Copyright (c) 2008-2013 Tristan Matthews <le.businessman@gmail.com>
//               2010 Harry van Haaren <harryhaaren@gmail.com>
//
// This file is part of Polygnome.
//
// Polygnome is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Polygnome is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Polygnome.  If not, see <http://www.gnu.org/licenses/>.
//
// Some of this code is based on samplerwidget.cpp from
// https://github.com/harryhaaren/Gtkmm-Custom-Widgets

#include "MTBeatWidget.h"
#include <gdkmm/general.h>
#include <cairomm/refptr.h>
#include <cmath>
#include <cassert>

MTBeatWidget::MTBeatWidget() :
    Gtk::DrawingArea(),
    meter_(1),
    beat_(0),
    active_(false),
    lineXpos_(0)
{
    set_size_request(240, 25);
    /*this->signal_button_press_event().connect(sigc::mem_fun(*this,
                &MTBeatWidget::on_button_press)); */
}

void MTBeatWidget::redraw()
{
	// force redraw of the entire widget.
	Glib::RefPtr<Gdk::Window> win = get_window();
	if (win) {
		Gdk::Rectangle r(0, 0, get_allocation().get_width(),
                         get_allocation().get_height());
		win->invalidate_rect(r, false);
	}
}

bool MTBeatWidget::getActive() const
{
    return active_;
}

void MTBeatWidget::setActive(bool active)
{
    active_ = active;
    redraw();
}

void MTBeatWidget::setBeat(int i)
{
    beat_ = i;
    redraw();
}

void MTBeatWidget::setMeter(int i)
{
    meter_ = i;
    assert(meter_ > 0);
    redraw();
}

bool MTBeatWidget::on_draw(const Cairo::RefPtr<Cairo::Context>& cr)
{
    assert(meter_ > 0);
    // this gets used a few times, and needs to be a double or we get roundoff errors
    const gdouble areaWidth = get_allocated_width();
    const gdouble beatWidth = areaWidth * (1.0 / meter_);
    const gint y0 = get_allocated_height() * 0.15;
    const gint y1 = get_allocated_height() * 0.75;

    // draw!
    for (gint i = 0; i < meter_; ++i) {
        Gdk::Color start_colour(beat_ == i and active_ ? "green" : "red");
        Gdk::Color end_colour(beat_ == i and active_ ? "#55FF55" : "#FF5555");

        const gint x0 =  beatWidth * i;
        const gint x1 = std::min(beatWidth * (i + 1), areaWidth);

        Cairo::RefPtr<Cairo::LinearGradient> background_gradient_ptr =
            Cairo::LinearGradient::create(x0, y0, x1, y0);

        // set gradient colors
        background_gradient_ptr->add_color_stop_rgb(0, start_colour.get_red_p(), start_colour.get_green_p(), start_colour.get_blue_p());

        background_gradient_ptr->add_color_stop_rgb(1, end_colour.get_red_p(), end_colour.get_green_p(), end_colour.get_blue_p());

        // draw a rectangle and fill with gradient
        cr->set_source(background_gradient_ptr);

        cr->rectangle(x0, y0, x1, y1);
        cr->fill();
    }

    if (active_) {
        // draw a line to denote beginning of current beat
        const Gdk::Color line_colour("black");

        cr->set_source_rgb(line_colour.get_red_p(), line_colour.get_green_p(), line_colour.get_blue_p());
        lineXpos_ = beat_ == 0 ? 0 : beat_ * beatWidth;
        cr->move_to(lineXpos_, 0);
        cr->line_to(lineXpos_, get_allocated_height());
        cr->stroke();
    }

    return true;
}
