// MTTicker.cpp
//
// Copyright (c) 2008-2013 Tristan Matthews <le.businessman@gmail.com>
//               2010 Alexandre Quessy <alexandre@quessy.net>
//
// based on:
// MPI_AudioSynth.cpp
// by Mark Zadel
//
// This file is part of Polygnome.
//
// Polygnome is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Polygnome is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Polygnome.  If not, see <http://www.gnu.org/licenses/>.
//
// Description:
// This class computes its output based on the parameters
// indicated by the user. The tick is an impulse input to
// a biquad filter. User will be able to change filter params,
// measure length in beats, and tempo of tick.

#include <cmath>
#include <stk/BiQuad.h>

#include "MTTicker.h"

namespace stk {

const StkFloat FILTER_RADIUS = 0.99;
const bool NORMALIZE = true;

MTTicker::MTTicker(RingBuffer<MTBeatEvent> &messageQueue, int id) :
    counter_(0), period_(0), measureLength_(0.0), impulse_(),
    filter_(), bpm_(1), ratio_(1.0), id_(id), lastOutput_(0.0), active_(false),
    beat_(0), cachedFrequency_(0), messageQueue_(messageQueue)
{
    addSampleRateAlert(this); // needed for bpm calculation
}

// Destructor
MTTicker::~MTTicker()
{
    removeSampleRateAlert(this);
}

void MTTicker::setRatio(double ratio)
{
    ratio_ = ratio;
    bpm(bpm_); // reset
}

void MTTicker::setActiveFlag(bool value)
{
    active_ = value;
}

void MTTicker::sampleRateChanged(StkFloat /*newRate*/, StkFloat /*oldRate*/)
{
    if (cachedFrequency_ > 0)
        filter_.setResonance(cachedFrequency_, FILTER_RADIUS, NORMALIZE);
    // update bpm with new samplerate (provided by call to sampleRate())
    bpm(bpm_);
}

bool MTTicker::isActive() const
{
    return active_;
}

void MTTicker::reset()
{
    lastOutput_ = 0.0;
}

void MTTicker::bpm(int newBpm)
{
    if (newBpm > 0) {
        const StkFloat INV_SECONDS_PER_MIN = 1.0 / 60.0;
        bpm_ = newBpm;
        measureLength_ = (1.0 / (bpm_ * (INV_SECONDS_PER_MIN) *
                         (1.0 / sampleRate())));
        period_ = round(measureLength_ / ratio_);
    }
}

void MTTicker::freq(int newFreq)
{
    if (newFreq > 0) {
        filter_.setResonance(newFreq, FILTER_RADIUS, NORMALIZE);
        cachedFrequency_ = newFreq;
    }
}

void MTTicker::gain(double newGain)
{
    if (newGain < 0.0)
        std::cerr << "Bad gain value " << newGain << std::endl;
    else
        impulse_.setGain(newGain);
}

void MTTicker::setMute(bool mute)
{
    impulse_.setMute(mute);
}
} // namespace stk
