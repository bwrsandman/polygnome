// MTBeatWidget.h
//
// Copyright (c) 2008-2013 Tristan Matthews <le.businessman@gmail.com>
//               2010 Alexandre Quessy <alexandre@quessy.net>
//
// This file is part of Polygnome.
//
// Polygnome is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Polygnome is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License

#ifndef MTBEATWIDGET_H_
#define MTBEATWIDGET_H_

#include <gtkmm.h>
#include "noncopyable.h"

class MTBeatWidget : public Gtk::DrawingArea {
    public:
        MTBeatWidget();
        void setMeter(int i);
        void setBeat(int i);
        void setActive(bool active);
        bool getActive() const;

    private:
        void redraw();
        bool on_draw(const Cairo::RefPtr<Cairo::Context>& cr);

        Gtk::Allocation allocation_;
        int meter_;
        int beat_;
        bool active_;
        int lineXpos_;

        NON_COPYABLE(MTBeatWidget);
};

#endif // MTBEATWIDGET_H_
