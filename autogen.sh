#!/bin/sh
if test ! -e NEWS ; then
touch NEWS
fi

HOOKS_DIR="../.git/hooks"
# install pre-commit hook for doing clean commits
if [ -d "$HOOKS_DIR" ];
then
    if test ! \( -x ${HOOKS_DIR}/pre-commit -a -L ${HOOKS_DIR}/pre-commit \);
    then
        rm -f ${HOOKS_DIR}/pre-commit
        ln -s ${HOOKS_DIR}/pre-commit.sample ${HOOKS_DIR}/pre-commit
    fi
fi

autoreconf --install
